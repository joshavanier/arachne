const Base = require('./base');
const Cal = require('../lib/cal');
const LogSet = require('../lib/set');
const Polyline = require('../lib/polyline');
const {cap, max, url} = require('../lib/utils');

module.exports = function (opts, logs) {
  Base.call(this, opts);

  function polychart (points) {
    const sort = logs.sortEntries();
    const span = sort.slice(sort.length - points);
    const durs = [];
    for (let i = 0, sl = span.length; i < sl; i++) {
      durs[durs.length] = +(new LogSet(span[i]).lh.toFixed(2));
    }
    return Polyline(durs, points);
  }

  function stats () {
    const ls = logs;
    const ll = ls.logs.length;
    if (ll === 0) return '';
    const sdg = new Date(ls.logs[0].s);
    const edg = new Date(ls.logs[ll - 1].e || ls.logs[ll - 2].e);
    const sd = new Cal(sdg).display();
    const ed = new Cal(edg).display();
    const ef = (ls.efficiency() / 100).toFixed(2);
    const lh = ls.logHours().toFixed(2);

    const start = `<time datetime="${sdg.toISOString()}">${sd}</time>`;
    const end = `<time datetime="${edg.toISOString()}">${ed}</time>`;

    return `<div>${start} to ${end}<br>${lh} hours<br>${ef} efficiency</div>`;
  }

  this.links = function () {
    const list = this.link;
    if (!list) return '';
    let nav = '';
    for (const item in list) {
      nav += `<li><a href="${list[item]}" target="_blank">${cap(item)}</a>`;
    }
    return `<nav><ul>${nav}</ul></nav>`;
  };

  this.info = function () {
    return logs.logs.length === 0 ? '' : `<aside>${polychart(120)}${stats()}${this.links()}</aside>`;
  };

  this.core = function () {
    return `${this.header()}<main id="mm">${this.content}</main>${this.info()}`;
  };
};
