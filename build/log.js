const fs = require('fs');
const {duration} = require('./lib/utils');

function Entry (attr) {
  Object.assign(this, attr);
  this.dur = duration(this.s, this.e);
};

module.exports = function (file) {
  const {log} = JSON.parse(fs.readFileSync(file, 'utf8'));
  this.index = {};
  this.raw = [];

  for (let i = 0, l = log.length; i < l; i++) {
    const pro = log[i].t.toUpperCase();
    const ent = new Entry(log[i]);
    if (!(pro in this.index)) this.index[pro] = [];
    this.index[pro][this.index[pro].length] = ent;
    this.raw[this.raw.length] = ent;
  }
};
