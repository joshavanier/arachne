const Builder = require('./build/builder');
const Database = require('./build/db');
const Factory = require('./build/factory');
const Log = require('./build/log');
const Tome = require('./build/lib/tome');

const fs = require('fs');
const os = require('os');

const logs = new Log(`${os.homedir()}/log.json`);
const databases = fs.readdirSync('./db');

let aggregate = '';
for (let i = 0, l = databases.length; i < l; i++) {
  const file = databases[i];
  if (file.endsWith('.tome')) {
    aggregate += fs.readFileSync(`./db/${file}`, 'utf8');
  }
}

console.time('Build');
const db = new Database(new Tome(aggregate));
new Builder(Factory(db, logs)).build();
console.timeEnd('Build');
